/**
 * Created by Invia on 02.06.2017.
 */
//Класс Machine которьій отвечает за включение холодильника
function Machine(power) {
    this._power = power;
    this._enabled = false;

    var self = this;

    this.enable = function() {
        self._enabled = true;
    };

    this.disable = function() {
        self._enabled = false;
    };
}

function Fridge(power) {
    // Формула вьічисления максимального количества едьі в холдильнике
    var maxFridgeQuant = power / 100;
    //Массив где хранится еда
    var food = [];
    //Подклюючаем класс Machine для включения холодильника
    Machine.apply(this, arguments);

    //Функция которая отвечает за добавление продуктов в холодильник
    this.addFood = function () {
        //Если количество добаляемьіх продуктов + продуктов которьіе уже находяться в холодильнике меньше или равно мощности холодильника / 100 и холодильник включен
        //Тогда можно еще добавить продуктов
        if (maxFridgeQuant >= food.length+arguments.length && this._enabled == true) {
            for (var i = 0; i < arguments.length; i++) {
                food.push(arguments[i]);
            };
        }
        //Если продуктов в холодильнике слишком много
        else if (maxFridgeQuant < food.length+arguments.length && this._enabled == true){
            throw new Error("Мало места в холодильнике");
        }
        //Если холодильник не включен
        else if (this._enabled == false) {
            throw new Error("Включите холодильник");
        };
    };
    //Просмотр продуктов в холодильнике
    this.getFood = function () {
        console.log(food.slice());
    };

    this.filterFood = function(filter) {
        return food.filter(filter);
    };

    this.removeFood = function(item) {
        var idx = food.indexOf(item);
        if (idx != -1) food.splice(idx, 1);
    };
    var selfDisable = this.disable;

    this.disable = function () {
        if (food.length != 0) {
            throw new Error("В холодильнике есть еда");
        }
        selfDisable();
    }

}

var fridgeTest = new Fridge(5000);

fridgeTest.enable();
fridgeTest.getFood();
fridgeTest.addFood('Капусточка');
fridgeTest.getFood();
fridgeTest.disable();
fridgeTest.enable();
fridgeTest.addFood('Картошечка');
fridgeTest.getFood();
fridgeTest.addFood('Сосичка');
fridgeTest.getFood();
fridgeTest.addFood({
    title: "котлета",
    calories: 100
});
fridgeTest.addFood({
    title: "сок",
    calories: 30
});
fridgeTest.addFood({
    title: "зелень",
    calories: 10
});
fridgeTest.addFood({
    title: "варенье",
    calories: 150
});

var dietItems = fridgeTest.filterFood(function(item) {
    return console.log(item.calories < 50);
});

fridgeTest.removeFood("нет такой еды");
console.log( fridgeTest.getFood().length );

dietItems.forEach(function(item) {
    console.log( item.title );
    fridgeTest.removeFood(item);
});

console.log( fridgeTest.getFood().length ); // 2*/

