/**
 * Created by Invia on 06.06.2017.
 */
var head = {
    glasses: 1
};

var table = {
    pen: 3

};
table.__proto__ = head;
var bed = {
    sheet: 1,
    pillow: 2
};
bed.__proto__ = table;
var pockets = {
    money: 2000
};
pockets.__proto__ = bed;

console.log(pockets.pen); //true
console.log(bed.glasses); //true
console.log(table.money); //true
