/**
 * Created by IDA on 20.02.2017.
 */
var menu = {
    width: 200,
    height: 300,
    title: "My menu"
};

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n)
}

function multX2(obj) {
    for (i in obj) {
        if (isNumeric(obj[i])) {
            obj[i] *= 2;
            return obj;
        }
    }
}

console.log(multX2(menu));
