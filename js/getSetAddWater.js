/**
 * Created by Invia on 01.06.2017.
 */

function CoffeeMachine(power, capacity) {
    var waterAmount = 0;

    var WATER_HEAT_CAPACITY = 4200;

    function getTimeToBoil() {
        return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
    }

    this.setWaterAmount = function(amount) {
        if (amount < 0) {
            throw new Error("Значение должно быть положительным");
        }
        if (amount > capacity) {
            throw new Error("Нельзя залить больше, чем " + capacity);
        }

        waterAmount = amount;
    };

    this.addWater = function(amount) {
        this.setWaterAmount(waterAmount+amount);
    };

    /* this.addWater = function(waterAdd) {
     *   if (waterAmount+waterAdd >= capacity) {
     *       throw new Error("Братан слей водьі плиз, а то вьіходит больше чем "+ capacity);
     *   }
     *
     *   if (waterAmount+waterAdd < 0) {
     *       throw new Error("У меня даже столько водьі нет");
     *   }
     *   waterAmount = waterAmount + waterAdd;
     }*/

    function onReady() {
        alert( 'Кофе готов!' );
    }

    this.run = function() {
        setTimeout(onReady, getTimeToBoil());
    };

}

var coffeeMachine = new CoffeeMachine(100000, 400);
coffeeMachine.addWater(200);
coffeeMachine.addWater(100);
coffeeMachine.addWater(1000); // Нельзя залить больше, чем 400
coffeeMachine.run();

