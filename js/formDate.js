/**
 * Created by Invia on 09.03.2017.
 */
//Напишите функцию formatDate(date), которая выводит дату date в формате дд.мм.гг:
//Например:
//    var d = new Date(2014, 0, 30); // 30 января 2014
//    alert( formatDate(d) ); // '30.01.14'
//    P.S. Обратите внимание, ведущие нули должны присутствовать, то есть 1 января 2001 должно быть 01.01.01, а не 1.1.1.

function formDate() {
    var d = new Date();
    var dat = d.getDate();
    var mon = d.getMonth()+1;
    var year = d.getFullYear()% 100 ;

    if (dat < 10) {
        dat = '0'+dat;
    }
    if (mon < 10) {
        mon = '0' + mon;
    }
    if (year < 10) {
        year = '0' + year;
    }

    var d = dat+ "-" +mon+ "-" +year;
    return d;

}



console.log(formDate());

