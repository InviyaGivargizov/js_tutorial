/**
 * Created by Invia on 01.06.2017.
 */
function CoffeeMachine(power, capacity) {

    var powerNew;

    this.getPowerCoffeeMachine = function () {
        powerNew = power;
        return console.log("Мощность кофеварки "+powerNew+" Ватт");
    };

    this.setWaterAmount = function(amount) {
        if (amount < 0) {
            throw new Error("Значение должно быть положительным");
        }
        if (amount > capacity) {
            throw new Error("Нельзя залить воды больше, чем " + capacity);
        }

        waterAmount = amount;
    };

    this.getWaterAmount = function() {
        return waterAmount;
    };

}

var createCoffeeMachine = new CoffeeMachine(700,4000);

createCoffeeMachine.setWaterAmount(1000);
createCoffeeMachine.setWaterAmount(500);
createCoffeeMachine.getPowerCoffeeMachine();
