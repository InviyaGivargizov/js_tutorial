/**
 * Created by Invia on 20.03.2017.
 */
function sumClosures(a) {
    return function (b) {
        return a + b;
    };
};

console.log(sumClosures(2)(3));