/**
 * Created by Invia on 01.06.2017.
 */

function User() {
    var FirstName, Surname;

    this.setFirstName = function(Name) {
        FirstName = Name;
    };

    this.setSurname = function(Name) {
        Surname = Name;
    };

    this.getFullName = function () {
        return FirstName+' '+Surname;
    };
}

var user = new User();
user.setFirstName("Петя");
user.setSurname("Иванов");

console.log( user.getFullName() ); // Петя Иванов


//  Напишите конструктор User для создания объектов:
//      С приватными свойствами имя firstName и фамилия surname.
//      С сеттерами для этих свойств.
//      С геттером getFullName(), который возвращает полное имя.
