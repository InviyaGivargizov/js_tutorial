/**
 * Created by Invia on 13.05.2017.
 */

function formatDate(date) {
    var toString = {}.toString;
    if (toString.apply(date).slice(8,-1) == 'Date') {
        var day = date.getDate(); //вьітянули дату
        if (day < 10) {
            day = '0' + day;
        };

        var month = date.getMonth()+1; //вьітянули месяц
        if (month < 10) {
            month = '0' + month;
        };

        var year = date.getFullYear() % 100; //вьітянули год
        if (year < 10) {
            year = '0' + year;

        };
        return console.log(day + '.' + month+'.'+ year); //вернули результат
    }
    if (toString.apply(date).slice(8,-1) == 'Number') {
        var fdate = new Date(date); //преобразовали число в дату

        var day = fdate.getDate(); //вьітянули дату
        if (day < 10) {
            day = '0' + day;
        };

        var month = fdate.getMonth()+1; //вьітянули месяц
        if (month < 10) {
            month = '0' + month;
        };

        var year = fdate.getFullYear() % 100; //вьітянули год
        if (year < 10) {
            year = '0' + year;

        };
        return console.log(day + '.' + month+'.'+ year); //вернули результат
    }
    if (toString.apply(date).slice(8,-1) == 'String') {
        var day = date.slice(-2); //вьітянули со строки дату
        var month = date.slice(-5,-3); //вьітянули со строки месяц
        var year = date.slice(2,4); //вьітянули со строки год

        return console.log(day + '.' +month+'.'+ year); //вернули результат
    }
    if (toString.apply(date).slice(8,-1) == 'Array') {
        var day = date.slice(2); //вьітянули с массива дату
        var month = date.slice(1,2); //вьітянули с массива месяц
        var year = ''+date.slice(0,1); //вьітянули с массива год перед єтим преоброзовав его в строку

        if (day < 10) {
            day = '0' + day;
        };

        if (month < 10) {
            month = '0' + month;
        };

        return console.log(day + '.' + month+'.'+ year.slice(2)); //вернули результат
    }
}

var date = new Date(2017,1,1);
var num = 1231235322546;
var str = '2017-02-01';
var arr = [2017,2,1];


formatDate(date);
formatDate(num);
formatDate(str);
formatDate(arr);