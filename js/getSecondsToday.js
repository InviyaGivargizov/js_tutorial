/**
 * Created by Invia on 09.03.2017.
 */

function getSecondsToday() {
    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    var now = new Date();
    var a = (now - today)/1000;
    return a;
}

//OR//

function getSecondsToday2() {
    var now = new Date();

    // создать объект из текущей даты, без часов-минут-секунд
    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    var diff = now - today; // разница в миллисекундах
    return Math.floor(diff / 1000); // перевести в секунды
}

console.log(getSecondsToday());
console.log(getSecondsToday2());

