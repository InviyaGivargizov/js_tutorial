/**
 * Created by Invia on 25.04.2017.
 */

function megaCalculator() {

    this.calculate = function (string) {
        this.firstVal = +string.charAt(0);
        this.symbStr = string.charAt(1);
        this.secondVal= +string.charAt(2);

        if (this.symbStr == '+') {
            return this.firstVal + this.secondVal;
        }
        else return this.firstVal - this.secondVal;
    }

}

var calc = new megaCalculator();

console.log(calc.calculate("9+9"));