/**
 * Created by Invia on 01.06.2017.
 */

function CoffeeMachine(power) {
    this.waterAmount = 0;

    var WATER_HEAT_CAPACITY = 4200;

    var self = this;

    function getBoilTime() {
        return self.waterAmount * WATER_HEAT_CAPACITY * 80 / power;
    }

    function onReady() {
        console.log( 'Coffee is ready!' );
    }

    this.run = function() {
        this.timerId = setTimeout(onReady, getBoilTime());
    }

    this.stop = function() {
        clearTimeout(self.timerId);
        console.log("Process interrupted!");
    }

}

var coffeeMachine = new CoffeeMachine(50000);
coffeeMachine.waterAmount = 200;

coffeeMachine.run(); //запуск процесса гтовки кофе
coffeeMachine.stop(); // отмена процесса готовки кофе
