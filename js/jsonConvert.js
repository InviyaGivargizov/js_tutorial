/**
 * Created by Invia on 26.04.2017.
 */

var leader = {
    name: "Василий Иванович",
    age: 35
};

var leaderJson = JSON.stringify(leader);
console.log(leaderJson);

var leaderAfterStylize = {
    name: "Василий Иванович",
    age: 35,
    roles: {
        isAdmin: false,
        isEditor: true
    }
};

var leaderJsonAfterStylize = JSON.stringify(leaderAfterStylize,55,4);
console.log(leaderJsonAfterStylize);