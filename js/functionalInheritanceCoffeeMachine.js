/**
 * Created by Invia on 01.06.2017.
 */
function Machine(params) {
    this._enabled = false;

    this.enable = function () {
        this._enabled = true;
    };
    this.disable = function () {
        this._enabled = false;
    };
}

function coffeeMachine(){
    Machine.apply(this,arguments);

    var waterAmount = 0;

    this.setWaterAmount = function(amount) {
        waterAmount = amount;
    };

    function onReady() {
        alert( 'Кофе готово!' );
    }

    this.run = function() {
        if (this._enabled == true) {
            setTimeout(onReady, 1000);
        }

        if (this._enabled == false) {
            throw new Error("Кофеварка вьіключена!");
        }
    };
}

var newCoffeeMachine = new coffeeMachine(10000);
newCoffeeMachine.enable();
newCoffeeMachine.run(); // ошибка, кофеварка выключена!