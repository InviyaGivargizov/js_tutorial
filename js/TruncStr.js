/**
 * Created by Invia on 20.02.2017.
 */

function truncate(str,maxlength) {

    var exceeded = "...";

    if (str.length >= maxlength) {
        return str.slice(0,(maxlength-3))+ exceeded;
    }
    else return str;
}

console.log(truncate("Subscription Validation Your WebStorm subscription expires on 25.02.2017.After this date you will no longer able to use WebStorm 2016.3.2 ",50))
console.log(truncate("Subscription Validation Your WebStorm",50));
