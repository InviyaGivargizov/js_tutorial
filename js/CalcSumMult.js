/**
 * Created by Invia on 13.03.2017.
 */

/*  Напишите функцию-конструктор Calculator, которая создает объект с тремя методами:
 *
 *  Метод read() запрашивает два значения при помощи prompt и запоминает их в свойствах объекта.
 *  Метод sum() возвращает сумму запомненных свойств.
 *  Метод mul() возвращает произведение запомненных свойств.
 *
 *  Пример использования:
 *  var calculator = new Calculator();
 *  calculator.read();
 *
 *  alert( "Сумма=" + calculator.sum() );
 *  alert( "Произведение=" + calculator.mul() );
 */

function calcSumMul() {

    this.read = function () {
        this.a = +prompt("Введите a?",0);
        this.b = +prompt("Введите b?",0)
    };

    this.sum = function () {
        return this.a + this.b;
    };

    this.mult = function (){
        return this.a * this.b;
    };
}

var calculator = new calcSumMul();
calculator.read();

console.log(calculator.sum());
console.log(calculator.mult());
