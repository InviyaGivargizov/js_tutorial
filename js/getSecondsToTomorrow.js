/**
 * Created by Invia on 09.03.2017.
 */
function getSecondsToTommorr() {
    var tommorrow = new Date();
    tommorrow.setDate(tommorrow.getDate() + 1,tommorrow.setHours(0),tommorrow.setMinutes(0),tommorrow.setSeconds(0));
    var now = new Date();
    var a = (tommorrow - now)/1000;
    return a;
}

//OR//

function getSecondsToTomorrow() {
    var now = new Date();

    // создать объект из завтрашней даты, без часов-минут-секунд
    var tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate()+1);

    var diff = tomorrow - now; // разница в миллисекундах
    return Math.round(diff / 1000); // перевести в секунды
}

console.log(getSecondsToTommorr());
console.log(getSecondsToTomorrow());
