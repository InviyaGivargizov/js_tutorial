/**
 * Created by Invia on 22.02.2017.
 */
var m = 'my-short-string';
var n = "list-style-image";
var l = "-webkit-transition";


function camelize(str) {
    var a = str.split('-');

    for (var i = 1; i < a.length; i++){
        a[i]=a[i].charAt(0).toUpperCase()+ a[i].slice(1);
    }

    s = a.join('');
    console.log(s);
}

camelize(m);
camelize(n);
camelize(l);
