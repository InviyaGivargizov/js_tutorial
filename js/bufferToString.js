/**
 * Created by Invia on 21.03.2017.
 */

function bufferToString() {
    var a = ' ';

    return {
        clear: function () {
            a = ' ';
        },

        buffit: function (val) {
            if (arguments.length == 0){
                return a;
            }
            a += val;
        }
    };
};

var buffer = bufferToString();

buffer.buffit('iop');
buffer.buffit('test');
buffer.buffit('done');

console.log(buffer.buffit());
console.log(buffer.clear());

buffer.buffit('done');
buffer.buffit('test');
buffer.buffit('iop');
console.log(buffer.buffit());