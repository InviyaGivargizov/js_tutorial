/**
 * Created by Invia on 27.02.2017.
 */

function getLastDayOfMonth(year, month) {
    var a = new Date(year,month + 1);
    var b = new Date(a.setDate(0));
    return b.getDate();
}

console.log(getLastDayOfMonth(2012,0));

//OR

function getLastDayOfMonth(year, month) {
    var date = new Date(year, month + 1, 0);
    return date.getDate();
}

alert( getLastDayOfMonth(2012, 0) ); // 31
alert( getLastDayOfMonth(2012, 1) ); // 29
alert( getLastDayOfMonth(2013, 1) ); // 28
